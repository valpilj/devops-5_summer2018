###############################################################################
#
# Modificate histogram-t1.awk script to provide percentage output instead of count
#
###############################################################################

# Counting the quantity of numbers in the range
{
  if ($1 >= 0 && $1 <= 9)
    count0 +=1;
  else if ($1 >9 && $1 <= 19)
    count1 +=1;
  else if ($1 > 19 && $1 <= 29)
    count2 +=1;
  else if ($1 > 29 && $1 <= 39)
    count3 +=1;
  else if ($1 > 39 && $1 <= 49)
    count4 +=1;
  else if ($1 > 49 && $1 <= 59)
    count5+=1;
  else if ($1 > 59 && $1 <= 69)
    count6+=1;
  else if ($1 > 69 && $1 <= 79)
    count7+=1;
  else if ($1 > 79 && $1 <= 89)
    count8+=1;
  else if ($1 > 89 && $1 <= 99)
    count9+=1;
  else
    count10+=1;
}

# Output for calculation as percentage
END {
 printf ("%s\t%d\t%0s\n", " 0 -  9:", count0/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count0/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "10 - 19:", count1/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count1/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "20 - 29:", count2/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count2/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "30 - 39:", count3/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count3/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "40 - 49:", count4/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count4/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "50 - 59:", count5/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count5/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "60 - 69:", count6/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count6/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "70 - 79:", count7/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count7/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "80 - 89:", count8/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count8/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "90 - 99:", count9/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count9/NR*100, "")))
 printf ("%s\t%d\t%0s\n", "    100:", count10/NR*100, gensub(/ /, "*", "g", sprintf("%*s", count10/NR*100, "")))
}
