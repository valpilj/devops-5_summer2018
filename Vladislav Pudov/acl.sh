#!/bin/bash
mkdir /tmp/Proj1 /tmp/Proj2 /tmp/Proj3
chmod 000 /tmp/Proj1
chmod 000 /tmp/Proj2
chmod 000 /tmp/Proj3
groupadd Dev
groupadd ItMan
groupadd Analitics
groupadd Proj1
groupadd Proj2
groupadd Proj3

useradd -G Dev,Proj2,Proj3 R1 -p ChahgeMe
useradd -G Dev,Proj1,Proj3 R2 -p ChahgeMe
useradd -G Dev,Proj1 R3 -p ChahgeMe
useradd -G Dev,Proj3 R4 -p ChahgeMe
useradd -G Dev,Proj1,Proj2 R5 -p ChahgeMe

useradd -G ItMan,Proj1,Proj2,Proj3 I1 -p ChahgeMe
useradd -G ItMan,Proj1,Proj2,Proj3 I2 -p ChahgeMe
useradd -G ItMan,Proj1,Proj2,Proj3 I3 -p ChahgeMe

useradd -G Analitics,Proj3 A1 -p ChahgeMe
useradd -G Analitics,Proj2 A2 -p ChahgeMe
useradd -G Analitics,Proj2 A3 -p ChahgeMe
useradd -G Analitics,Proj1,Proj3 A4 -p ChahgeMe

setfacl -m u:R2:rwx /tmp/Proj1
setfacl -m u:R3:rwx /tmp/Proj1
setfacl -m u:R5:rwx /tmp/Proj1
setfacl -m u:A4:r-x /tmp/Proj1
chown root:Proj1 Proj1
sudo chmod g+ws,o= Proj1

setfacl -m u:R1:rwx /tmp/Proj2
setfacl -m u:R5:rwx /tmp/Proj2
setfacl -m u:A1:rwx /tmp/Proj2
setfacl -m u:A2:r-x /tmp/Proj2
setfacl -m u:A3:r-x /tmp/Proj2
chown root:Proj2 Proj2
sudo chmod g+ws,o= Proj2

setfacl -m u:R1:rwx /tmp/Proj3
setfacl -m u:R2:rwx /tmp/Proj3
setfacl -m u:R4:rwx /tmp/Proj3
setfacl -m u:A1:r-x /tmp/Proj3
setfacl -m u:A4:r-x /tmp/Proj3
chown root:Proj3 Proj3
sudo chmod g+ws,o= Proj3

chmod +t /tmp/Proj1
chmod +t /tmp/Proj2
chmod +t /tmp/Proj3
